<h1 align='center'>
Step Project Forkio
</h1>

<p align='center'>
The second step project after the completion of Module 2: Advanced HTML & CSS
</p>
<br>

### Technologies utilized:

- HTML
- CSS
- SASS / SCSS
- JavaScript
- NPM
- Gulp
- Gulp Packages:
  - browser-sync
  - gulp-autoprefixer
  - gulp-clean
  - gulp-clean-css
  - gulp-concat
  - gulp-imagemin
  - gulp-sass
  - gulp-terser
  - sass
- Git
- Markdown
- GitHub Pages

### Project Participants:

**1. Alina Palyvoda**

**2. Ahmed Fahmy**

<br>

### Participant's Roles:

##### Student 1 : Alina Palyvoda

- Site Header Section
- Testomonials Section
- Gulp packages and tasks
- Readme file
- GitHub Pages

##### Student 2 : Ahmed Fahmy

- Revolutionary-Editor Section
- Here-is-what-you-get Section
- Fork-Subscription Section
- Gulp packages and tasks
- Readme file
- GitHub Pages

### Project Links:

The GitHub Pages technology was utilized to publish the project on a public domain routed and published on the internet.

[Student1 Repository](https://alinapalyvoda.github.io/Project-Forkio/) **OR** [Student2 Repository](https://ahmed-fahmy-dev.github.io/Project-Forkio)

<br>

### Project Creation Workflow:

_The project's creation workflow is summarized briefly in the below points:_

- Remote Repository with main branch created and managed by GIT version control system.

- GitFlow workflow was adopted to manage the repository Git Branches.
- Develop branch was created from the main branch and updated with the initial project assets and file structure.
- Each Developer created feature branches to work individually on different website sections.
- Each feature branch upon completion was pushed and merge requests were created and assigned to the other developer for revision and approval before merging back into the develop branch (GitFlow workflow).
- Upon project completion the develop branch was merged into the marin branch.
- Gulp tool and packages were used to build the project.
- SASS 7-1 Module was used for the SASS folder file structure.

![GitFlow image](./dist/images/readme/gitflow-workflow.png)
