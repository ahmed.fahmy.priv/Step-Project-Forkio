const burgerMenuBtn = document.querySelector(".header__burger-btn");
const menuCloseBtn = document.querySelector(".header__close-btn");
const dropdownMenuList = document.querySelector(".header__menu-wrapper");
const minTabletWidth = 481;

function closeMenu() {
    burgerMenuBtn.classList.remove("hide");
    menuCloseBtn.classList.add("hide");
    dropdownMenuList.classList.add("hide")
}

function menuVisibility() {
    if (window.innerWidth >= minTabletWidth) {
        dropdownMenuList.classList.remove("hide")
    } else {
        closeMenu()
    }
}

menuVisibility();

window.addEventListener("resize", () => {
    menuVisibility()
})

document.querySelector(".header__dropdown")
    .addEventListener("click", (event) => {
            if (event.target.tagName === "IMG") {
                burgerMenuBtn.classList.toggle("hide");
                menuCloseBtn.classList.toggle("hide");
                dropdownMenuList.classList.toggle("hide")
            }
        }
    )

document.addEventListener("click", (event) => {
    if (window.innerWidth < minTabletWidth) {
        if (!event.target.closest(".header__menu-wrapper") &&
            (!event.target.classList.contains("header__burger-btn"))) {
            closeMenu()
        }
    }
})